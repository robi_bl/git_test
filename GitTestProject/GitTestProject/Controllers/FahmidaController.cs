﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GitTestProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FahmidaController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<string > Get()
        {
            string spring = "Spring";
            string [] season =  new string[4]{ "Summer", "Winter","Autumn", spring };
            return season;
        }
    }
}